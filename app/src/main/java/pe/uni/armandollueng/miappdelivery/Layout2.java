package pe.uni.armandollueng.miappdelivery;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class Layout2 extends AppCompatActivity {

    TextView textViewTitulo;
    Button buttonEnviar;
    Spinner spinner;
    EditText editTextNombre;
    EditText editTextDomicilio;
    RadioButton radioButtonEfectivo;
    RadioButton radioButtonTarjeta;
    ArrayAdapter<CharSequence> adapter;
    LinearLayout linearLayout;
    SharedPreferences sharedPreferences;
    String nombreDes;
    String domicilioDes;
    boolean efectivo;
    boolean Tarjeta;
    int posicion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout2);

        textViewTitulo = findViewById(R.id.titulo_plato_seleccionado);
        buttonEnviar = findViewById(R.id.button_enviar_pedido);
        spinner = findViewById(R.id.spinner);
        editTextNombre = findViewById(R.id.edit_text_nombre_destinatario);
        editTextDomicilio = findViewById(R.id.edit_text_domicilio_destinatario);
        radioButtonEfectivo = findViewById(R.id.radio_button_efectivo);
        radioButtonTarjeta = findViewById(R.id.radio_button_tarjeta);
        linearLayout = findViewById(R.id.linear_layout);


        Intent intent = getIntent();
        String titulo = intent.getStringExtra("TITULO");

        adapter = ArrayAdapter.createFromResource(this, R.array.cantidades, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        textViewTitulo.setText(titulo);

        buttonEnviar.setOnClickListener(v -> {
            String nombre = editTextNombre.getText().toString();
            String domicilio = editTextDomicilio.getText().toString();

            if(verificar(nombre, domicilio,radioButtonEfectivo.isChecked(),radioButtonTarjeta.isChecked())){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle(R.string.dialog_title).setMessage(R.string.dialog_msg).
                        setNegativeButton(R.string.No, (dialog, which) -> {
                        }).setPositiveButton(R.string.yes, (dialog, which) -> {
                            Intent intent2 = new Intent(Layout2.this, Layout1.class);
                            startActivity(intent2);
                        }).show();
                alertDialog.create();
            }else{
                Snackbar.make(linearLayout, R.string.msg_snack_bar,Snackbar.LENGTH_LONG).setAction("cerrar", v1 -> {

                }).show();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    protected boolean verificar(String destinatario, String domicilio, boolean efectivo, boolean tarjeta){
        if(!destinatario.equals("")){
            if(!domicilio.equals("")){
                return efectivo || tarjeta;
            }
        }
        return false;
    }

    protected void saveData(){
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        nombreDes= editTextNombre.getText().toString();
        domicilioDes = editTextDomicilio.getText().toString();
        efectivo = radioButtonEfectivo.isChecked();
        Tarjeta = radioButtonTarjeta.isChecked();

        Toast.makeText(getApplicationContext(), "tus datos estan guardados", Toast.LENGTH_LONG).show();

    }
}