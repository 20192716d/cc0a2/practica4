package pe.uni.armandollueng.miappdelivery;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> descripcion;
    ArrayList<String> titulo;
    ArrayList<Integer> imagen;

    public GridAdapter(Context context, ArrayList<String> descripcion,ArrayList<String> titulo, ArrayList<Integer> imagen) {
        this.context = context;
        this.descripcion = descripcion;
        this.titulo = titulo;
        this.imagen = imagen;
    }

    @Override
    public int getCount() {
        return titulo.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_layout, parent, false);
        ImageView imageView = view.findViewById(R.id.image_view_plato);
        TextView textViewTitulo = view.findViewById(R.id.text_view_titulo_animal);
        TextView textViewDescripcion = view.findViewById(R.id.text_view_descripcion_plato);
        imageView.setImageResource(imagen.get(position));
        textViewTitulo.setText(titulo.get(position));
        textViewDescripcion.setText(descripcion.get(position));
        return view;
    }
}
