package pe.uni.armandollueng.miappdelivery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class Layout1 extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> titulo = new ArrayList<>();
    ArrayList<String> descripcion = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout1);

        gridView = findViewById(R.id.grid_view);
        fillArrays();

        GridAdapter gridAdapter = new GridAdapter(this, titulo, descripcion, image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {

            String tituloSeleccionado;

            Intent intent = new Intent(Layout1.this, Layout2.class);

            tituloSeleccionado = titulo.get(position);

            intent.putExtra("TITULO", tituloSeleccionado);

            startActivity(intent);
        });

    }

    private void fillArrays(){
        titulo.add("adobo");
        titulo.add("arroz con pollo");
        titulo.add("carapulcra");
        titulo.add("causa");
        titulo.add("ceviche");
        titulo.add("cuy chactado");
        titulo.add("juane");
        titulo.add("papa a la huancaina");

        descripcion.add("adobo");
        descripcion.add("arroz con pollo");
        descripcion.add("carapulcra");
        descripcion.add("causa");
        descripcion.add("ceviche");
        descripcion.add("cuy chactado");
        descripcion.add("juane");
        descripcion.add("papa a la huancaina");

        image.add(R.drawable.adobo);
        image.add(R.drawable.arroz_con_pollo);
        image.add(R.drawable.carapulcra);
        image.add(R.drawable.causa);
        image.add(R.drawable.ceviche);
        image.add(R.drawable.cuy_chactado);
        image.add(R.drawable.juane);
        image.add(R.drawable.papa_a_la_huancaina);
    }

}